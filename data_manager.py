# -------------------------------------------------------------------------------------
#   import this file to load dataset using data_manager.load_dataset()
# -------------------------------------------------------------------------------------

import tensorflow as tf
import tensorflow_addons as tfa
import numpy as np
import os
import sys
import pathlib
import csv
import shutil
import math
import random

from constant import IMG_WIDTH, IMG_HEIGHT, FLIP_RATE, SAMPLE_SET, DEVELOPMENT_SET, TRAINING_SET, SORTED_SET, AUGMENTATION_SET, DEMO_SET

def load_dataset_list(dataset):
    return load_csv(dataset)

def load_csv(dataset):
    ds = []
    with open(dataset.LABEL_DIR.value, newline = '') as file:
        for i, row in enumerate(csv.reader(file)):
            if (dataset == DEVELOPMENT_SET or dataset == TRAINING_SET or dataset == DEMO_SET) and i == 0:
                continue
            ds.append([row[0], get_labels(dataset, row[1])])
        file.close()
        return ds

def get_labels(dataset, grade):
    return dataset.CLASS.value.index(grade)

def decode_image(img):
    img = tf.image.decode_jpeg(img, channels = 3)
    img = tf.image.convert_image_dtype(img, tf.float32)
    return tf.image.resize(img, [IMG_WIDTH, IMG_HEIGHT])

def rotate_image(img, degree):
    return tfa.image.rotate(img, math.radians(degree), interpolation = 'BILINEAR')

def flip_image(img):
    return tf.image.flip_left_right(img)

def read_image(path, preprocess = True):
    img = tf.io.read_file(path)
    img = decode_image(img)
    if preprocess:
        img = rotate_image(img, 15)
        if random.random() < FLIP_RATE:
            img = flip_image(img)
    return img

def get_image(path, preprocess = True):
    return read_image(path, preprocess)

def sort_image(dataset):
    list_ds = load_dataset_list(dataset)
    os.makedirs(SORTED_SET.IMG_DIR_A.value + '/1', exist_ok = True)
    os.makedirs(SORTED_SET.IMG_DIR_B.value + '/2', exist_ok = True)
    os.makedirs(SORTED_SET.IMG_DIR_C.value + '/3', exist_ok = True)
    for fn, label in list_ds:
        output_dir = SORTED_SET.BASE_DIR.value + '/' + chr(ord('A') + label) + '/' + str(label + 1) + '/' + fn
        shutil.copy(dataset.IMG_DIR.value + '/' + fn, output_dir)

def load_dataset(dataset, preprocess = True, predict = False):
    images = []
    labels = []
    files = []
    if dataset == SORTED_SET:
        if dataset == SORTED_SET:
            print('sorted set is the combination of (sample, devolopment, training) set used for DCGAN,')
            print('use other sets instead.')
            exit()
    elif dataset == AUGMENTATION_SET:
        dir = pathlib.Path(dataset.IMG_DIR.value)
        list_ds = tf.data.Dataset.list_files(str(dir/'*/*'), shuffle = True)
        for fp in list_ds:
            img = get_image(fp, preprocess)
            parts = tf.strings.split(fp, os.path.sep)
            label = parts[-2].numpy().decode('ascii')
            label = get_labels(dataset, label)
            file = parts[-1].numpy().decode('ascii')
            images.append(img.numpy())
            labels.append(label)
            files.append(file)
        labels = tf.one_hot(labels, 3)
        return np.array(images), np.array(labels), np.array(files)
    elif dataset == DEMO_SET:
        dir = pathlib.Path(dataset.IMG_DIR.value)
        list_ds = tf.data.Dataset.list_files(str(dir/'*.jpg'), shuffle = False)
        if not predict:
            data_list = load_dataset_list(dataset)
        for fp in list_ds:
            img = get_image(fp, preprocess)
            parts = tf.strings.split(fp, os.path.sep)
            file = parts[-1].numpy().decode('ascii')
            images.append(img.numpy())
            files.append(file)
            if not predict:
                for fn, label in data_list:
                    if file == fn + '.jpg':
                        labels.append(label)
                        break
        if not predict:
            labels = tf.one_hot(labels, 3)
        return np.array(images), np.array(labels), np.array(files)
    else:   # primitive datasets
        list_ds = load_dataset_list(dataset)
        for fn, label in list_ds:
            img = get_image(tf.convert_to_tensor(dataset.IMG_DIR.value + '/' + fn), preprocess)
            images.append(img.numpy())
            labels.append(label)
            files.append(fn)
        labels = tf.one_hot(labels, 3)
        return np.array(images), np.array(labels), np.array(files)