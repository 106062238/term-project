# -------------------------------------------------------------------------------------
#   run this file to demo. demo.py --pred - predict only | demo.py - with evaluation
# -------------------------------------------------------------------------------------

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
import tensorflow as tf
import numpy as np
import argparse
import csv

import basic_DL as bdl
import test_basic_DL as tbdl
import data_manager as dm
from constant import MEAN, DEVIATION, DEMO_SET, MODELS_SAVE_PATH

def parse_argument():
    parser = argparse.ArgumentParser()
    parser.add_argument('--pred', action = 'store_true', help = 'predict only')
    return parser.parse_args()

def load_demo_dataset(dataset, preprocess = False, predict = False):
    try:
        x, y, file_names = dm.load_dataset(dataset, preprocess, predict)
    except:
        print(str(dataset) + ' dataset not found.')
        exit()
    return x, y, file_names

def write_predicted_label(predict_dict):
    os.makedirs(DEMO_SET.OUTPUT_DIR.value, exist_ok = True)
    with open(DEMO_SET.OUTPUT_DIR.value + '/' + DEMO_SET.FILE_NAME.value, 'w', newline = '') as f:
        writer = csv.DictWriter(f, fieldnames = DEMO_SET.COLUMNS.value)
        writer.writeheader()
        key = DEMO_SET.COLUMNS.value[0]
        value = DEMO_SET.COLUMNS.value[1]
        for k, v in predict_dict.items():
            writer.writerow({key: k, value: v})

def make_dictionary(file_names, pred):
    d = {}
    for fn, p in zip(file_names, pred):
        file_name = fn.split('.')[0]
        if file_name.isdecimal() and int(file_name) < 10:
            d[file_name] = p + 1
    for fn, p in zip(file_names, pred):
        file_name = fn.split('.')[0]
        if file_name.isdecimal() and int(file_name) >= 10:
            d[file_name] = p + 1
    return d

if __name__ == '__main__':

    # check compatibility
    print('tensorflow version:', tf.version.VERSION)
    bdl.check_compatibility()

    # parse argument from command line
    print('parsing argument...')
    opt = parse_argument()

    # load demo dataset for evaluating
    print('loading demo dataset...')
    demo_x, demo_y, demo_file_names = load_demo_dataset(DEMO_SET, preprocess = False, predict = opt.pred)

    # normalize data
    print('normalizing image...')
    mean = np.array(MEAN)
    deviation = np.array(DEVIATION)
    demo_x = bdl.normalize_data(demo_x, mean, deviation)

    # load the model from file
    model = bdl.load_model(MODELS_SAVE_PATH.BASIC_DL)
    if not model is None:
        model.summary()

        # load the model history from file to draw learning curves
        tbdl.plot_history(MODELS_SAVE_PATH.BASIC_DL)

        if not opt.pred:
            # evaluate with the restored model
            print('evaluate with the loaded model...')
            loss, acc = model.evaluate(demo_x, demo_y, batch_size = 1, verbose = 1)

            print('\nloss:', loss)
            print('accuracy:', acc * 100, '%')

        # predict with the restored model
        print('\npredict with the loaded model...')
        pred_y = model.predict(demo_x, batch_size = 1, verbose = 0)

        # calculate statistics
        pred_y = tbdl.logits_to_label(pred_y)
        if not opt.pred:
            label_y = tbdl.prob_to_label(demo_y) 
            statistics = tf.math.confusion_matrix(label_y, pred_y).numpy()

        # write the predicted label to file
        print('write the results...')
        predict_dict = make_dictionary(demo_file_names, pred_y)
        write_predicted_label(predict_dict)

        if not opt.pred:
            # display confusion matrix
            tbdl.display_confusion_matrix(statistics)