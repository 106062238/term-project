# -------------------------------------------------------------------------------------
#   run this file before training or using DCGAN model
# -------------------------------------------------------------------------------------

import data_manager as dm
from constant import SAMPLE_SET, DEVELOPMENT_SET, TRAINING_SET

if __name__ == '__main__':
    dm.sort_image(SAMPLE_SET)
    dm.sort_image(DEVELOPMENT_SET)
    dm.sort_image(TRAINING_SET)