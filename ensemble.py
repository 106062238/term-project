# -------------------------------------------------------------------------------------
#   run this file to perform ensemble classifying
# -------------------------------------------------------------------------------------

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
import tensorflow as tf
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier
from sklearn.ensemble import AdaBoostClassifier
import numpy as np

import basic_DL as bdl
import test_basic_DL as tbdl
from constant import MEAN, DEVIATION, SAMPLE_SET, DEVELOPMENT_SET, TRAINING_SET, AUGMENTATION_SET

# there is a bug in the original tensorflow keras model wrapper,
# so we need a custom extended wrapper for keras model
class KerasClassifierExtend(KerasClassifier):
    def fit(cls, x, y, sample_weight = None, **kwargs):
        return super().fit(x, y)

def build_keras_classifier():
    return bdl.create_model(shape, 3)

if __name__ == '__main__':

    # check compatibility
    print('tensorflow version:', tf.version.VERSION)
    bdl.check_compatibility()

    # load datasets
    print('loading dataset... this takes some time')
    train_x, train_y, train_file_names = bdl.load_dataset(TRAINING_SET)
    sample_x, sample_y, sample_file_names = bdl.load_dataset(SAMPLE_SET)
    aug_x, aug_y, aug_file_names = bdl.load_dataset(AUGMENTATION_SET)
    dev_x, dev_y, dev_file_names = bdl.load_dataset(DEVELOPMENT_SET, preprocess = False)
    train_y = tbdl.prob_to_label(train_y)
    sample_y = tbdl.prob_to_label(sample_y)
    aug_y = tbdl.prob_to_label(aug_y)
    dev_y = tbdl.prob_to_label(dev_y)

    train_x = np.concatenate([train_x, sample_x, aug_x])
    train_y = np.concatenate([train_y, sample_y, aug_y])
    train_file_names = np.concatenate([train_file_names, sample_file_names, aug_file_names])

    sample_x = None
    sample_y = None
    sample_file_names = None
    aug_x = None
    aug_y = None
    aug_file_names = None

    # normalize data
    print('normalizing data...')
    mean = np.array(MEAN)
    deviation = np.array(DEVIATION)
    train_x = bdl.normalize_data(train_x, mean, deviation)
    dev_x = bdl.normalize_data(dev_x, mean, deviation)

    # set configuration
    shape = train_x[0].shape
    class_num = 3
    batch_size = 32
    epochs = 20
    n_estimators = 15

    # load basic DL model and wraps it with scikit-learn classifier API
    estimator = KerasClassifierExtend(build_fn = build_keras_classifier, epochs = epochs, batch_size = batch_size, verbose = 1)

    # create adaboost classifier
    print('initializing AdaBoost Classifier...')
    ab_clf = AdaBoostClassifier(base_estimator = estimator, n_estimators = n_estimators)

    # train ensembled model
    print('training classifier...')
    ab_clf.fit(train_x, train_y)

    # evaluate accuracy
    print('testing model...')
    train_accuracy = ab_clf.score(train_x, train_y)
    test_accuracy = ab_clf.score(dev_x, dev_y)
    print('training accuracy:', train_accuracy)
    print('testing accuracy:', test_accuracy)