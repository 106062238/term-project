# -------------------------------------------------------------------------------------
#   run this file with DCGAN_output.py --cuda --type A | B | C to generate fake image
# -------------------------------------------------------------------------------------

from __future__ import print_function
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils

import DCGAN_128 as dg
from constant import DCGAN_PARAM

def adjust_opt_for_output(opt):
    opt.batchSize = 1
    opt.niter = 1000
    opt.lr = 0
    return opt

if __name__ == '__main__':

    ngpu = DCGAN_PARAM.NGPU.value   # number of GPUs to use
    nz = DCGAN_PARAM.NZ.value       # size of the latent z vector
    ngf = DCGAN_PARAM.NGF.value
    ndf = DCGAN_PARAM.NDF.value
    nc = DCGAN_PARAM.NC.value

    # parse argument from command line
    print('parsing argument...')
    opt = dg.parse_argument()
    opt = adjust_opt_for_output(opt)
    #print(opt)

    # setup directories for input and output
    print('setup directories...')
    _type = opt.type.upper()
    opt.dataroot, opt.netG, opt.netD, opt.outf = dg.setup_directories(_type, generate = True)
    dg.make_output_directory(opt.outf)

    cudnn.benchmark = True

    # check settings
    if torch.cuda.is_available() and not opt.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")
    
    if opt.dataroot is None and str(opt.dataset).lower() != 'fake':
        raise ValueError("`dataroot` parameter is required for dataset \"%s\"" % opt.dataset)

    # set configurations
    batch_size = opt.batchSize
    if opt.dataset == 'folder':
        # folder dataset
        dataset = dset.ImageFolder(root = opt.dataroot,
                                   transform = transforms.Compose([
                                   transforms.Resize(opt.imageSize),
                                   transforms.CenterCrop(opt.imageSize),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                                ]))
    else:
        print('do not change --dataset')
        exit()

    assert dataset
    dataloader = torch.utils.data.DataLoader(dataset,
                                             batch_size = batch_size,
                                             shuffle = True,
                                             num_workers = int(opt.workers)
                                        )

    # setup device
    device = torch.device("cuda:0" if opt.cuda else "cpu")

    # initialize net
    netG = dg.initialize_netG(ngpu, device, opt)
    #print(netG)
    
    for epoch in range(opt.niter):
        # set random seed
        opt.manualSeed = random.randint(1, 10000)
        print("Random Seed: ", opt.manualSeed)
        random.seed(opt.manualSeed)
        torch.manual_seed(opt.manualSeed)

        noise = torch.randn(batch_size, nz, 1, 1, device = device)
        fake = netG(noise)

        vutils.save_image(fake.detach(),
                          ('%s/fake_samples_' + _type + '_%03d.png') % (opt.outf, epoch),
                          normalize = True)