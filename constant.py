from enum import Enum

IMG_WIDTH = 224
IMG_HEIGHT = 224
FLIP_RATE = 0.5

MEAN = [0.485, 0.456, 0.406]
DEVIATION = [0.229, 0.224, 0.225]

class DCGAN_PARAM(Enum):
    NGPU = 1
    NZ = 100
    NGF = 64
    NDF = 32
    NC = 3

class SAMPLE_SET(Enum):
    IMG_DIR = './AIMango_sample/sample_image'
    LABEL_DIR = './AIMango_sample/label.csv'
    CLASS = ['等級A', '等級B', '等級C']

class DEVELOPMENT_SET(Enum):
    IMG_DIR = './C1-P1_Train Dev_fixed/C1-P1_Dev'
    LABEL_DIR = './C1-P1_Train Dev_fixed/dev.csv'
    CLASS = ['A', 'B', 'C']

class TRAINING_SET(Enum):
    IMG_DIR = './C1-P1_Train Dev_fixed/C1-P1_Train'
    LABEL_DIR = './C1-P1_Train Dev_fixed/train.csv'
    CLASS = ['A', 'B', 'C']

class SORTED_SET(Enum):
    BASE_DIR = './sorted_image'
    IMG_DIR_A = BASE_DIR + '/A'
    IMG_DIR_B = BASE_DIR + '/B'
    IMG_DIR_C = BASE_DIR + '/C'

class AUGMENTATION_SET(Enum):
    IMG_DIR = './augmentation'
    CLASS = ['A', 'B', 'C']

class DEMO_SET(Enum):
    IMG_DIR = './Program Exam_Mango Grade Classification'
    LABEL_DIR = IMG_DIR + '/Program Exam_Answer.csv'
    OUTPUT_DIR = './demo'
    FILE_NAME = 'Team24_Demo.csv'
    COLUMNS = ['ImageID', 'PredictedLabel']
    CLASS = ['1', '2', '3']

class MODELS_SAVE_PATH(Enum):
    BASE_DIR = './models'
    BASIC_DL = BASE_DIR + '/basic_DL'
    ENSEMBLE = BASE_DIR + '/ensemble'
    DCGAN_D_A = BASE_DIR + '/DCGAN/netD_A.pth'
    DCGAN_D_B = BASE_DIR + '/DCGAN/netD_B.pth'
    DCGAN_D_C = BASE_DIR + '/DCGAN/netD_C.pth'
    DCGAN_G_A = BASE_DIR + '/DCGAN/netG_A.pth'
    DCGAN_G_B = BASE_DIR + '/DCGAN/netG_B.pth'
    DCGAN_G_C = BASE_DIR + '/DCGAN/netG_C.pth'
    BASE_DIR_TMP = './GAN_out'
    DCGAN_TMP_A = BASE_DIR_TMP + '/A'
    DCGAN_TMP_B = BASE_DIR_TMP + '/B'
    DCGAN_TMP_C = BASE_DIR_TMP + '/C'

class CHECKPOINT_SAVE_PATH(Enum):
    BASIC_DL = './checkpoints/basic_DL'