# -------------------------------------------------------------------------------------
#   run this file to load the best weights trained so far into
#   a basic deep learning model and predict with testing data
# -------------------------------------------------------------------------------------

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import json

import basic_DL as bdl
import data_manager as dm
from constant import MEAN, DEVIATION, DEVELOPMENT_SET, MODELS_SAVE_PATH

def plot_history(path):
    print('try plotting learning curves...')
    try:
        history = load_history(path)
        print('close plots to continue...')
        bdl.draw_learning_curves(history)
    except:
        print('cannot find saved history file.')

def load_history(path):
    return json.load(open(path.value + '/history.json', 'r'))

def logits_to_label(pred_y):
    pred_y = logits_to_prob(pred_y)
    return prob_to_label(pred_y)

def logits_to_prob(pred_y):
    return tf.nn.softmax(pred_y).numpy()

def prob_to_label(y):
    return np.argmax(y, axis = 1)

def display_confusion_matrix(statistics):
    class_num = statistics.shape[0]
    print_matrix_title('confusion matrix')

    print('     guess ', end = '')
    for i in range(class_num):
        print('|', end = '')
        print('{:^5}'.format(str(i + 1)), end = '')
    new_line()

    print(' true      ', end = '')
    for i in range(class_num):
        print('|', end = '')
        print('     ', end = '')
    new_line()

    for i in range(class_num):
        print_row_line(class_num)
        new_line()
        print(' {:^5}     '.format(str(i + 1)), end = '')
        for j in range(class_num):
            print('|', end = '')
            print('{:^5}'.format(str(statistics[i][j])), end = '')
        new_line()
        
def print_matrix_title(title):
    print(title + ':')
    print('================================')

def print_row_line(num):
    print('-----------', end = '')
    for i in range(num):
        print('-', end = '')
        print('-----', end = '')

def new_line():
    print()

if __name__ == '__main__':

    # check compatibility
    print('tensorflow version:', tf.version.VERSION)
    bdl.check_compatibility()

    # load development dataset for evaluating
    print('loading testing dataset...')
    dev_x, dev_y, dev_file_names = bdl.load_dataset(DEVELOPMENT_SET, preprocess = False)

    # normalize data
    print('normalizing image...')
    # mean and deviation defined by official baseline
    mean = np.array(MEAN)
    deviation = np.array(DEVIATION)
    dev_x = bdl.normalize_data(dev_x, mean, deviation)

    # load the model from file
    model = bdl.load_model(MODELS_SAVE_PATH.BASIC_DL)
    if not model is None:
        model.summary()

        # load the model history from file to draw learning curves
        plot_history(MODELS_SAVE_PATH.BASIC_DL)

        # evaluate with the restored model
        print('evaluate with the loaded model...')
        loss, acc = model.evaluate(dev_x, dev_y, batch_size = 1, verbose = 1)

        print('\nloss:', loss)
        print('accuracy:', acc * 100, '%')

        # predict with the restored model
        print('\npredict with the loaded model...')
        pred_y = model.predict(dev_x, batch_size = 1, verbose = 0)

        # calculate statistics
        pred_y = logits_to_label(pred_y)
        label_y = prob_to_label(dev_y) 
        statistics = tf.math.confusion_matrix(label_y, pred_y).numpy()

        # display confusion matrix
        display_confusion_matrix(statistics)