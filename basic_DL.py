# -------------------------------------------------------------------------------------
#   run this file to train the basic deep learning model
# -------------------------------------------------------------------------------------

import os
# do not print tensorflow debug info
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.optimizers import Adam, RMSprop
from tensorflow.keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split
import numpy as np
import matplotlib.pyplot as plt
import json

import data_manager as dm
from constant import MEAN, DEVIATION, SAMPLE_SET, DEVELOPMENT_SET, TRAINING_SET, AUGMENTATION_SET, MODELS_SAVE_PATH, CHECKPOINT_SAVE_PATH

def check_compatibility():
    # check if tensorflow version support cuda
    if tf.test.is_built_with_cuda():
        # log info to check whether the computations are actually executed on GPU
        #tf.debugging.set_log_device_placement(True)
        print('cuda supported.')
        gpus = tf.config.experimental.list_physical_devices('GPU')
        if gpus:
            try:
                for gpu in gpus:
                    # set memory limit for GPUs to prevent crash
                    tf.config.experimental.set_memory_growth(gpu, True)
                logical_gpus = tf.config.experimental.list_logical_devices('GPU')
                print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
            except RuntimeError as e:
                print(e)

def load_dataset(dataset, preprocess = True):
    try:
        x, y, file_names = dm.load_dataset(dataset, preprocess = preprocess)
    except:
        print(str(dataset) + ' dataset not found.')
        exit()
    return x, y, file_names

def load_model(path):
    print('try loading existing model...')
    try:
        model = tf.keras.models.load_model(path.value)
        print('model loaded...')
    except:
        model = None
        print('no previous model found...')
    return model

def normalize_data(data, mean, deviation):
    data -= mean
    data /= deviation
    return data

def create_model(shape, class_num):
    print('initializing model...')
    model = Sequential([
        Conv2D(16, 3, padding = 'same', activation = 'relu', input_shape = shape, data_format = 'channels_last'),
        MaxPooling2D(),
        Dropout(0.4),
        Conv2D(32, 3, padding = 'same', activation = 'relu'),
        MaxPooling2D(),
        Conv2D(64, 3, padding = 'same', activation = 'relu'),
        MaxPooling2D(),
        Conv2D(128, 3, padding = 'same', activation = 'relu'),
        MaxPooling2D(),
        Conv2D(256, 3, padding = 'same', activation = 'relu'),
        MaxPooling2D(),
        Dropout(0.4),
        Flatten(),
        Dense(64, activation = 'relu'),
        Dense(class_num)
    ])

    print('compiling model...')
    model.compile(
        optimizer = 'adam',
        loss = tf.keras.losses.CategoricalCrossentropy(from_logits = True),
        metrics = ['accuracy']
    )
    return model

def draw_learning_curves(history):
    epochs_range = range(len(history['loss']))
    acc = history['accuracy']
    val_acc = history['val_accuracy']
    loss = history['loss']
    val_loss = history['val_loss']

    plt.figure(figsize = (8, 8))
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label = 'Training Accuracy')
    plt.plot(epochs_range, val_acc, label = 'Validation Accuracy')
    plt.legend(loc = 'lower right')
    plt.title('Training and Validation Accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    plt.legend(loc = 'upper right')
    plt.title('Training and Validation Loss')
    plt.show(block = False)
    plt.pause(5)
    plt.close()

def save_model(path, model, history):
    print('saving the model with better result...')
    model.save(path.value)
    history_dict = history.history
    json.dump(history_dict, open(path.value + '/history.json', 'w'))

if __name__ == '__main__':

    # ----------------------------------------------------------------------------------

    # note: the development is based on tensorflow v2.2 (with gpu support), cuda 10.1, and cudnn 7.6.5 compatible with cuda 10.1.

    # check compatibility
    print('tensorflow version:', tf.version.VERSION)
    check_compatibility()

    # ----------------------------------------------------------------------------------
    
    # prepare training dataset
    
    # use official training dataset as training data
    print('preparing dataset... this takes some time')
    # training dataset
    train_x, train_y, train_file_names = load_dataset(TRAINING_SET)
    sample_x, sample_y, sample_file_names = load_dataset(SAMPLE_SET)
    aug_x, aug_y, aug_file_names = load_dataset(AUGMENTATION_SET)

    train_x = np.concatenate([train_x, sample_x, aug_x])
    train_y = np.concatenate([train_y, sample_y, aug_y])
    train_file_names = np.concatenate([train_file_names, sample_file_names, aug_file_names])

    sample_x = None
    sample_y = None
    sample_file_names = None
    aug_x = None
    aug_y = None
    aug_file_names = None

    # load development dataset for testing
    print('loading testing dataset...')
    dev_x, dev_y, dev_file_names = load_dataset(DEVELOPMENT_SET, preprocess = False)

    # ----------------------------------------------------------------------------------

    # normalize data

    print('normalizing image...')
    # mean and deviation defined by official baseline
    mean = np.array(MEAN)
    deviation = np.array(DEVIATION)
    train_x = normalize_data(train_x, mean, deviation)

    # normalize dev dataset
    print('normalizing testing dataset...')
    dev_x = normalize_data(dev_x, mean, deviation)

    # ----------------------------------------------------------------------------------

    # set configuration for model and training

    # run more sessions to get better results.
    # note: if you are lacking VRAM, lower the batch_size and epochs.

    print('setting configuration...')
    shape = train_x[0].shape
    class_num = train_y.shape[1]
    batch_size = 32
    epochs = 50
    sessions = 3
    previous_acc = 0

    # check if model already exist
    model = load_model(MODELS_SAVE_PATH.BASIC_DL)
    if not model is None:
        model.summary()
        try:
            test_loss, previous_acc = model.evaluate(dev_x, dev_y, batch_size = 1, verbose = 0)
            print('previous accuracy:', previous_acc)
        except:
            previous_acc = 0
            print('model not compatible...')

    print('dataset ready...')

    # ----------------------------------------------------------------------------------

    # train model to find better weights for 'sessions' number of rounds

    for i in range(sessions):
        print('\nsession: [%d/%d]' % (i + 1, sessions))

        # set up model
        model = create_model(shape, class_num)
        if i == 0:
            model.summary()

    # ----------------------------------------------------------------------------------

        # train model

        print('training model...')
        # set up checkpoint
        cp_callback = ModelCheckpoint(filepath = CHECKPOINT_SAVE_PATH.BASIC_DL.value,
                                      save_weights_only = False,
                                      monitor = 'val_accuracy',
                                      mode = 'max',
                                      save_best_only = True)

        # train
        history = model.fit(train_x, train_y, batch_size = batch_size, epochs = epochs, validation_data = (dev_x, dev_y), callbacks = [cp_callback])

        try:
            draw_learning_curves(history.history)
        except:
            print('plot closed...')
    
    # ----------------------------------------------------------------------------------

        # test model

        print('testing model...')
        # load best checkpoint
        model = load_model(CHECKPOINT_SAVE_PATH.BASIC_DL)

        # test
        test_loss, test_accuracy = model.evaluate(dev_x, dev_y, batch_size = 1, verbose = 1)

        print('\ntest_loss:', test_loss)
        print('test_accuracy:', test_accuracy * 100, '%')

        # save model if the result is better
        if test_accuracy > previous_acc:
            previous_acc = test_accuracy
            save_model(MODELS_SAVE_PATH.BASIC_DL, model, history)