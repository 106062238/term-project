# -------------------------------------------------------------------------------------
#   run this file to generate augmented images (traditional method)
# -------------------------------------------------------------------------------------

# Import
import torch
import argparse
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torchvision.utils import save_image
from customDataset import MangoDataset

import DCGAN_128 as dg
from constant import TRAINING_SET, AUGMENTATION_SET

def parse_argument():
    # Parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--resize', type = int, default = 256, help = 'resize the input image')
    parser.add_argument('--crop', type = int , default = 224, help = 'size of the output image')
    parser.add_argument('--brightness', type = float, default = 0.5, help = 'brightness of image')
    parser.add_argument('--rotate', type = float, default = 45, help = 'degrees of rotating image')
    parser.add_argument('--gray', type = float, default = 0.2, help = 'value of grayscale')
    return parser.parse_args()

if __name__ == '__main__':

    opt = parse_argument()
    #print(opt)

    # Load Data
    my_transform = transforms.Compose([
        transforms.ToPILImage(),
        transforms.Resize((opt.resize, opt.resize)),
        transforms.RandomCrop((opt.crop, opt.crop)),
        transforms.ColorJitter(brightness = opt.brightness),
        transforms.RandomRotation(degrees = opt.rotate),
        transforms.RandomHorizontalFlip(p = 0.5),
        transforms.RandomVerticalFlip(p = 0.05),
        transforms.RandomGrayscale(p = opt.gray),
        transforms.ToTensor()
    ])

    dataset = MangoDataset(csv_file = TRAINING_SET.LABEL_DIR.value, root_dir = TRAINING_SET.IMG_DIR.value, transform = my_transform)
    
    dg.make_output_directory(AUGMENTATION_SET.IMG_DIR.value + '/A')
    dg.make_output_directory(AUGMENTATION_SET.IMG_DIR.value + '/B')
    dg.make_output_directory(AUGMENTATION_SET.IMG_DIR.value + '/C')

    # Generate
    for i, (img, label) in enumerate(dataset):
        path = (AUGMENTATION_SET.IMG_DIR.value + '/' + label + '/traditional_samples_' + label + '_%03d.png') % (i)
        vutils.save_image(img, path, normalize = True)